package interfacePatterns.facade;

import java.time.LocalDateTime;

public class ParkingSpace {
	private int number;
	private LocalDateTime inDate;
	private LocalDateTime outDate;
	
	private Client client; //Dependency injection
	
	public Client getClient() {
		return client;
	}

	public void setClient(Client client) {
		this.client = client;
	}

	public int getNumber() {
		return number;
	}

	public void setNumber(int number) {
		this.number = number;
	}

	public LocalDateTime getInDate() {
		return inDate;
	}

	public void setInDate(LocalDateTime inDate) {
		this.inDate = inDate;
	}

	public LocalDateTime getOutDate() {
		return outDate;
	}

	public void setOutDate(LocalDateTime outDate) {
		this.outDate = outDate;
	}

}
