package interfacePatterns.facade;

public class App {

	public static void main(String[] args) {
		ParkingSpaceControlFacade control = new ParkingSpaceControlFacade(
				new Client(),
				new ParkingSpace()
		);
		
		control.setClientName("Mirosvaldo Borges");
		control.setLicensePlate("CBC34565");
		control.setSpaceNumber(265);
		control.rentParkingSpace();
		
		try {
			Thread.sleep(3000L);
		} catch (Exception e) {
			System.out.println("Can't wait.");
		}
		
		control.unlockParkingSpace();
	}
}
