package interfacePatterns.facade;

import java.time.LocalDateTime;
import java.time.ZoneId;

public class ParkingSpaceControlFacade {

	/**
	 * Facade offers an unique interface for a set of interfaces of a subsystem.
	 * It defines an interface of up-level that becomes the subsystem easiest to
	 * use.
	 */

	private Client client;
	private ParkingSpace parkingSpace;
	private final ZoneId brZone = ZoneId.of("Brazil/East");

	public ParkingSpaceControlFacade(Client client, ParkingSpace space) {
		this.client = client;
		this.parkingSpace = space;
	}

	public void setClientName(String name) {
		client.setName(name);
	}

	public String getClientName() {
		return client.getName();
	}

	public final String getLicensePlate() {
		return client.getLicensePlate();
	}

	public void setLicensePlate(String licensePlate) {
		client.setLicensePlate(licensePlate);
	}

	public final int getSpaceNumber() {
		return parkingSpace.getNumber();
	}

	public void setSpaceNumber(int number) {
		parkingSpace.setNumber(number);
	}

	public void rentParkingSpace() {
		if (parkingSpace.getClient() == null) {
			parkingSpace.setClient(client);
			parkingSpace.setInDate(LocalDateTime.now(brZone));
			System.out.println("Done.");
		} else {
			System.out.println("Space Locked.");
		}
	}

	public void unlockParkingSpace() {
		if (parkingSpace.getClient() != null) {
			parkingSpace.setOutDate(LocalDateTime.now(brZone));
			parkingSpace.setClient(null);
			parkingSpace.setInDate(null);
			System.out.println("Successfull unlocked now.");
		} else {
			System.out.println("Vacancy!");
		}
	}
}
