package interfacePatterns.adapter;

public abstract class Polygon {

	private float centerX = 0;
	private float centerY = 0;

	public abstract double area();

	public float getCenterX() {
		return centerX;
	}

	public void setCenterX(float centerX) {
		this.centerX = centerX;
	}

	public float getCenterY() {
		return centerY;
	}

	public void setCenterY(float centerY) {
		this.centerY = centerY;
	}

}
