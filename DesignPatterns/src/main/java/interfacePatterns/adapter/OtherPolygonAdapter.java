package interfacePatterns.adapter;

import java.util.List;

public class OtherPolygonAdapter extends Polygon {

	/**
	 * The objective of adapter is convert the interface of a class in other
	 * interface expected by clients. Adapter allows the communication between
	 * classes that can't work together due to the incompatibility between their
	 * interfaces.
	 */

	private OtherPolygonalForm otherForm = new OtherPolygonalForm();

	public List<Float> getDimensions() {
		return otherForm.getDimensions();
	}

	public void fillDimensions(List<Float> dimensions) {
		otherForm.fillDimensions(dimensions);
	}

	@Override
	public double area() {
		return otherForm.calculateArea();
	}

}
