package interfacePatterns.adapter;

public class Square extends Polygon {

	private float size;

	@Override
	public double area() {
		return size * size;
	}

	public float getSize() {
		return size;
	}

	public void setSize(float size) {
		this.size = size;
	}

}
