package interfacePatterns.adapter;

import java.util.ArrayList;
import java.util.List;

public class OtherPolygonalForm {

	private List<Float> dimensions = new ArrayList<>();
	
	public Double calculateArea(){
		float sum = 0;
		for(Float f : dimensions){
			sum+=f;
		}
		return (double) sum;
	}

	public List<Float> getDimensions() {
		return dimensions;
	}

	public void fillDimensions(List<Float> dimensions) {
		this.dimensions = dimensions;
	}
	
}
