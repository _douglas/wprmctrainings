package interfacePatterns.adapter;

public class Triangle extends Polygon {

	private float base = 0;
	private float high = 0;

	@Override
	public double area() {
		return (float) ((0.5) * base * high);
	}

	public float getBase() {
		return base;
	}

	public void setBase(float base) {
		this.base = base;
	}

	public float getHigh() {
		return high;
	}

	public void setHigh(float high) {
		this.high = high;
	}

}
