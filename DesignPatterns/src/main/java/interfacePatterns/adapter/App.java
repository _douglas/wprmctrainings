package interfacePatterns.adapter;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class App {

	public static void main(String[] args) {
		OtherPolygonAdapter form = new OtherPolygonAdapter();
		
		Random rand = new Random();
		List<Float> dimensions = new ArrayList<>();
		
		int max = 50, min = 3;
		int factor = max - min + 1;
		int numberOfDimensions = rand.nextInt() % factor;
		
		while (numberOfDimensions > 0) {
			Float value = rand.nextFloat() + 10;
			dimensions.add(value);
			numberOfDimensions--;
			System.out.print(value + " | ");
			System.out.println();
		}
		
		form.fillDimensions(dimensions);
		Polygon p = form;
		
		System.out.print("Area:\t");
		System.out.println(p.area());
		
	}
}
