package builder.abstractFactory;

public class ColarBijuteria extends Colar {
	private int tipoMaterial;

	public int getTipoMaterial() {
		return tipoMaterial;
	}

	public void setTipoMaterial(int tipoMaterial) {
		this.tipoMaterial = tipoMaterial;
	}

}
