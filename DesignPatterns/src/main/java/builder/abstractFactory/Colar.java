package builder.abstractFactory;

public abstract class Colar {

	private int qtdPreciosa;

	public int getQtdPreciosa() {
		return qtdPreciosa;
	}

	public void setQtdPreciosa(int qtdPreciosa) {
		this.qtdPreciosa = qtdPreciosa;
	}

}
