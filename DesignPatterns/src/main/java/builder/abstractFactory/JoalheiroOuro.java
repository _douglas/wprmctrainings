package builder.abstractFactory;

public class JoalheiroOuro extends Joalheiro {

	@Override
	public Colar novoColar() {
		return new ColarOuro();
	}

	@Override
	public Anel novoAnel() {
		return new AnelOuro();
	}

}
