package builder.abstractFactory;

public abstract class Joalheiro {

	/**
	 * Should provide an interface to create families of objects related or
	 * dependent without specifies their concrete class. This pattern should be
	 * used when the program should be configured by families of classes. All
	 * classes of a same family should be used together.
	 * 
	 * @return
	 */

	public abstract Colar novoColar();

	public abstract Anel novoAnel();
}
