package builder.abstractFactory;

public class JoalheiroBijuteria extends Joalheiro{
	@Override
	public Colar novoColar(){
		return new ColarBijuteria();
	}

	@Override
	public Anel novoAnel() {
		return new AnelBijuteria();
	}
	
}
