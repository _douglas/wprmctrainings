package responsabilityPatterns.observer;

public abstract class Tax {

	abstract public void update(double value);
}
