package responsabilityPatterns.observer;

//Is a labor tax
public class Payroll extends LaborTax{

	private double salContrib = 0;

	public double getSalContrib() {
		return salContrib;
	}

	//Change in object occurs
	public void setSalContrib(double salContrib) {
		this.salContrib = salContrib;
		super.notify(salContrib);
	}
	
	
}
