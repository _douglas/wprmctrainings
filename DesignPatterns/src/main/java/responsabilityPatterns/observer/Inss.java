package responsabilityPatterns.observer;

public class Inss extends Tax {

	private double inssValue = 0d;

	@Override
	public void update(double value) {
		inssValue = value * 0.1;
	}

	public double getInssValue() {
		return inssValue;
	}

}
