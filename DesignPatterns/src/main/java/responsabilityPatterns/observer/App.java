package responsabilityPatterns.observer;

public class App {
	
	/**
	 * The observer defines a dependency 1-*
	 * between two objects, for when an object changes its
	 * state, all of its dependents were notified and
	 * updated automatically.
	 * @param args
	 */

	public static void main(String[] args) {
		Payroll payroll = new Payroll();
		Pis pis = new Pis();
		Inss inss = new Inss();

		payroll.addTax(pis);
		payroll.addTax(inss);

		payroll.setSalContrib(1000);

		System.out.println("pis = " + pis.getPisValue());
		System.out.println("inss = " + inss.getInssValue());
	}
}
