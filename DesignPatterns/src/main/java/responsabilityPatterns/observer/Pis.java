package responsabilityPatterns.observer;

public class Pis extends Tax{

	private double pisValue = 0D;

	@Override
	public void update(double value) {
		pisValue = value * 0.03;
	}
	
	public double getPisValue(){
		return pisValue;
	}
}
