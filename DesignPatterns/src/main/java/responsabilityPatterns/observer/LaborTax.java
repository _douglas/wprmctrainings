package responsabilityPatterns.observer;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public abstract class LaborTax {

	//1-* Dependency injection
	private List<Tax> taxes = new ArrayList<>();

	//Notification
	protected void notify(double value) {
		Iterator<Tax> taxIt = taxes.iterator();
		for (Tax t : taxes) {
			taxIt.next().update(value);
		}
	}
	
	public void addTax(Tax tax){taxes.add(tax);}
	public Tax removeTax(int index){
		return taxes.remove(index);
	}
	public Iterator<Tax> getIteratorTax(){
		return taxes.iterator();
	}
}
