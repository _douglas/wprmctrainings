package responsabilityPatterns.singleton;

public class App {

	public static void main(String[] args) {
		GlobalCounter c1, c2, c3;
//		c1 = new GlobalCounter(); // does not compile
		c1 = GlobalCounter.getInstance();
		System.out.println("c1 = " + c1.getNumber());
		
		c2 = GlobalCounter.getInstance();
		c3 = GlobalCounter.getInstance();
		
		if(c2 == c3){
			System.out.println("Both counters are the same");
		}
		
		c2.increase();
		System.out.println("c2 = "+ c2.getNumber());
		System.out.println("c3 = "+ c3.getNumber());
		
		c3.increase();
		System.out.println("c2 = "+ c2.getNumber());
		System.out.println("c3 = "+ c3.getNumber());
	}
}
